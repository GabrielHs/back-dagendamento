<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusBanco = array(
            'lista' =>
                array(
                    0 =>
                        array(
                            'id' => 1,
                            'type' => 'disponivel',
                        ),
                    1 =>
                        array(
                            'id' => 2,
                            'type' => 'indisponivel',
                        ),
                    2 =>
                        array(
                            'id' => 3,
                            'type' => 'sem previsão',
                        ),
                    3 =>
                        array(
                            'id' => 4,
                            'type' => 'aprovado',
                        ),
                    4 =>
                        array(
                            'id' => 5,
                            'type' => 'reprovado',
                        ),
                    5 =>
                        array(
                            'id' => 6,
                            'type' => 'ausente',
                        ),
                    6 =>
                        array(
                            'id' => 7,
                            'type' => 'liberado',
                        ),
                    7 =>
                        array(
                            'id' => 8,
                            'type' => 'encerrado',
                        ),
                    8 =>
                        array(
                            'id' => 9,
                            'type' => 'em atendimento',
                        ),
                ),
        );

        foreach ($statusBanco['lista'] as $status) {
            DB::table('status')->insert([
                'id' => $status['id'],
                'type' => $status['type'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
