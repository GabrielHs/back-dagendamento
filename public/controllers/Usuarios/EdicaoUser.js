$(function () {
    $('form[name="formEdicaoUser"]').submit(function (event) {
        event.preventDefault();

        let id = $('input[name="idUser"]').val();
        let divMsg = $('.messagesRetorno')

        $.ajax({
            url: `/admin/usuarios/${id}`,
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
        }).done(function (response) {
            if (response.sucess === false) {
                if (Array.isArray(response.errors)) {
                    response.errors.forEach(item => {
                        divMsg.append(item).addClass('alert alert-danger')
                    })
                } else {
                    divMsg.append(response.errors).addClass('alert alert-danger')
                }
            } else {

                alert('Usuario cadastro com sucesso, você será redirecionado')
                setTimeout(() =>{
                    window.location = '/admin/usuarios';
                    // window.history.go(0)

                }, 2000)
            }


        })
    })


});
