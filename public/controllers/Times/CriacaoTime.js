$(function () {
    $('form[name="formCriacaoTime"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url:'/admin/times/create',
            type:'post',
            data:$(this).serialize(),
            dataType:'json',
        }).done(function (response) {
            console.log(response)
            if (response.sucess === false) {
                if (Array.isArray(response.errors)) {
                    response.errors.forEach(item => {
                        $("#msgCriacao").append(item).addClass('alert alert-danger').removeAttr("style");
                    })
                } else {
                    $("#msgCriacao").append(response.errors).addClass('alert alert-danger').removeAttr("style");
                }
            } else {


                alert('time criado com sucesso, você será redirecionado')
                setTimeout(() =>{
                    window.location = '/admin/times';
                    // window.history.go(0)

                }, 2000)
                // alert('Usuario cadastro com sucesso')
                // window.history.go(0)
            }
        })
    })


});
