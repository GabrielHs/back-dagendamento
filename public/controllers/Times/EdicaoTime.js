$(function () {
    $('form[name="formEdicaoTime"]').submit(function (event) {
        event.preventDefault();
        let id = $('input[name="idTime"]').val();

        $.ajax({
            url: `/admin/times/${id}`,
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
        }).done(function (response) {
            console.log(response)
            if (response.sucess === false) {
                alert('Não foi foi possivel atualizar time')

            } else {
                // alert('Usuario atualizado com sucesso')
                // window.history.go(0)
                alert('time atualizado com sucesso, você será redirecionado')
                setTimeout(() =>{
                    window.location = '/admin/times';
                    // window.history.go(0)

                }, 2000)
            }
        })
    })


});
