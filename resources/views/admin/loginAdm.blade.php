@extends('shared.login')

@section('title', 'login')
@section('content')
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <form class="form-signin" method="post" action="{{url('admin/login')}}">
        @csrf
        <img class="mb-4" src="{{ asset('imgs/shared/logo_login.png') }}" alt="logo" width="100" height="100">
        <h1 class="h3 mb-3 font-weight-normal">Faça seu login</h1>

        @foreach($errors->all() as $error)

            <div class="alert alert-danger" role="alert">
                {{$error}}
            </div>
        @endforeach
        <label for="inputEmail" class="sr-only">Email</label>
        <input name="email" type="email" id="inputEmail" class="form-control"
               placeholder="Seu e-mail usuario@dominio.com"
               required
               autofocus>
        <label for="inputPassword" class="sr-only">Senha</label>
        <input name="password" type="password" required id="inputPassword" class="form-control" placeholder="Sua senha">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Acessar</button>
        <p class="mt-5 mb-3 text-muted">2020</p>
    </form

@endsection


