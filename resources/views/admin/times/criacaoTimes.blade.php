@extends('shared.painel')
@section('title', 'Adminstração Usuário (criação)')

@section('content')

    <h3 class="h3">Criar time tecnico</h3>
    <form name="formCriacaoTime">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Nome do time</label>
                <input name="name" type="text" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Adicionar tecnicos a equipe</label>
                <div class="form-check">
                    @foreach($users as $user)
                    <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" name="usersToTime[]" value="{{$user->id}}"> - {{$user->name}}<br/>
                    @endforeach
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Cadastrar</button>
    </form>

    <div style="display: none" id="msgCriacao" class="alert alert-secondary" role="alert">

    </div>

    <script src="{{asset('controllers/Times/CriacaoTime.js')}}"></script>
@endsection
