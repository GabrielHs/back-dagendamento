@extends('shared.painel')
@section('title', 'Adminstração Usuário (edição)')

@section('content')

    <h3 class="h3">Edição time tecnico</h3>
    <form name="formEdicaoTime">
        @csrf
        <input type="hidden" name="idTime" value="{{$time->id}}">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Nome do time</label>
                <input name="name" value="{{$time->name}}" type="text" class="form-control">
            </div>
{{--            <div class="form-group col-md-6">--}}
{{--                <label>Adicionar tecnicos a equipe</label>--}}
{{--                <div class="form-check">--}}
{{--                    @foreach($users as $user)--}}
{{--                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" name="usersToTime[]" value="{{$user->id}}"> - {{$user->name}}<br/>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>

        <button type="submit" class="btn btn-primary">Atualizar</button>
    </form>



    <script src="{{asset('controllers/Times/EdicaoTime.js')}}"></script>
@endsection
