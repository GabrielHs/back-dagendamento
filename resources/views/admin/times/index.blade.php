@extends('shared.painel')
@section('title', 'Adminstração Usuário')


@section('content')
    <a href="{{route('admin.times.create')}}" class="btn btn-outline-primary">Criação de time tecnico</a>
    <h2>Usuários e suas equipes</h2>
    <div class="container">
        <form method="get" action="{{url('admin/times/pesquisa')}}">

            <div class="row">
                <div class="col">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" placeholder="Nome do time">
                        <button type="submit" class="btn btn-secondary">
                            <span data-feather="search"></span>
                        </button>
                    </div>

                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Nome do time</th>
                <th>Criado em</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>

            @foreach($times as $time)
                <tr>
                    <td>{{$time->id}}</td>
                    <td>{{$time->name}}</td>
                    <td>{{date("d/m/Y", strtotime($time->created_at))}}</td>
                    <td>
                        <a class="btn btn-warning" href="{{ url("/admin/times/$time->id") }} ">Editar</a>
                        <a class="btn btn-danger" href="{{ url("/admin/times/delete/$time->id") }}">Excluir</a>
                    </td>
                </tr>
            @endforeach

            {{ $times->links() }}


            @if(!count($times))
                <tr>
                    <td colspan="6"><h4>Não encontrado {{isset($busca) ? isset($busca)  : ''}} em nossos registros na
                            busca</h4></td>
                </tr>
            @endif

            </tbody>
        </table>
    </div>


@endsection
