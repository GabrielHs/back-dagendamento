@extends('shared.painel')
@section('title', 'Adminstração Usuário (criação)')

@section('content')

    <h3 class="h3">Criação de usuários</h3>
    <form name="formCriacaoUser">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Nome</label>
                <input name="name" type="text" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">Email</label>
                <input name="email" type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Senha</label>
                <input name="password" type="password" class="form-control" id="inputPassword4">
            </div>
            <div class="form-group col-md-6">
                <label>Função</label>
                <select name="role" class="form-control">
                    <option selected value="operador">Operador</option>
                    <option value="tecnico">Tecnico</option>
                    <option value="admin">Administrador</option>
                </select>
            </div>
        </div>

{{--        <div class="form-row">--}}

{{--        </div>--}}
        <button type="submit" class="btn btn-primary">Cadastrar</button>
    </form>


    <script src="{{asset('controllers/Usuarios/CriacaoUser.js')}}"></script>
@endsection
