@extends('shared.painel')
@section('title', 'Adminstração Usuário (edição)')

@section('content')

    <h3 class="h3">Edicao de usuário</h3>
    <form name="formEdicaoUser">
        @csrf
        <input type="hidden" name="idUser" value="{{$user->id}}">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Nome</label>
                <input name="name" value="{{$user->name}}" type="text" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">Email</label>
                <input name="email" value="{{$user->email}}" type="email" class="form-control" id="inputEmail4">
            </div>
{{--            <div class="form-group col-md-6">--}}
{{--                <label for="inputPassword4">Senha</label>--}}
{{--                <input name="password" type="password" class="form-control" id="inputPassword4">--}}
{{--            </div>--}}

            <div class="form-group col-md-6">
                <label>Função cadastrada</label>
                <select name="roleCadastrada" class="form-control">
                @foreach($user->roles as $role)
                    <option value="{{$role->id}}">{{$role->type}}</option>
                @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label>Atualizar Função</label>
                <select name="roleAtualizar" class="form-control">
                    <option selected="" value="operador">Operador</option>
                    <option value="tecnico">Tecnico</option>
                    <option value="admin">Administrador</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label>Status da Função</label>
                <select name="statusUser" class="form-control">
                @foreach($status as $sts)
                    <option value="{{$sts->id}}">{{$sts->type}}</option>
                @endforeach
                </select>
            </div>

            <div class="messagesRetorno"></div>
        </div>

        {{--        <div class="form-row">--}}

        {{--        </div>--}}
        <button type="submit" class="btn btn-success">Atualizar</button>
    </form>


    <script src="{{asset('controllers/Usuarios/EdicaoUser.js')}}"></script>
@endsection
