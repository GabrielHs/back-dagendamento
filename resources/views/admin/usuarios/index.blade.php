@extends('shared.painel')
@section('title', 'Adminstração Usuário')

@section('content')
    <script src="{{asset('controllers/Usuarios/index.js')}}"></script>
    <a href="{{route('admin.usuarios.create')}}" class="btn btn-outline-primary">Criação de usuário</a>
    <h2>Usuarios cadastrados</h2>
    <div class="container">
        <form method="get" action="{{url('admin/times/pesquisa')}}">

            <div class="row">
                <div class="col">
                    <div class="input-group">
                        <input type="text"  class="form-control" name="q" placeholder="Nome">
                        <button type="submit" class="btn btn-secondary">
                            <span data-feather="search"></span>
                        </button>
                    </div>

                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Função(ões)</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>

                            <select class="form-control" style="width: 200px">
                        @foreach($user->roles as $role)
                                <option value="">{{$role->type}} - {{$role->statusUser->type}}</option>
                        @endforeach
                            </select>

                    </td>
                    <td>
                        <a class="btn btn-warning" href="{{ url("/admin/usuarios/$user->id") }}">Editar</a>
                        <a class="btn btn-danger" href="{{ url("/admin/usuarios/delete/$user->id") }}">Excluir</a>
                    </td>
                </tr>
            @endforeach
            {{ $users->links() }}


            @if(!count($users))
                <tr>
                    <td colspan="6"><h4>Não encontrado {{isset($busca) ? isset($busca)  : ''}} em nossos registros na busca</h4></td>
                </tr>
            @endif



            </tbody>
        </table>
    </div>

    {{--    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">--}}
    {{--        ...--}}
    {{--    </div>--}}

@endsection
