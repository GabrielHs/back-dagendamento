<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function statusUser()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

}
