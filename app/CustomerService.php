<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerService extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class);
    }


    public function calleds()
    {
        return  $this->hasMany(Called::class);
    }

}
