<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class);


    }


    public function schedulings()
    {
        return $this->hasMany(Scheduling::class);
    }
}
