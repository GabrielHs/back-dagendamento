<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Called extends Model
{
    public function customer_services()
    {
        return   $this->belongsTo(CustomerService::class);

    }


    public function schedelings()
    {
        return  $this->hasMany(Scheduling::class);
    }
}
