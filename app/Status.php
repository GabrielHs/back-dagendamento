<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{

    protected $table = 'status';

    public function roles()
    {
        return $this->belongsTo(Role::class, 'status_id');
    }
}
