<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheduling extends Model
{
    public function users()
    {
        return  $this->belongsTo(User::class);
    }



    public function teams()
    {
        return  $this->belongsTo(Team::class);

    }



    public function calleds()
    {
        return  $this->belongsTo(Called::class);

    }
}
