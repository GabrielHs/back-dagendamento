<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsuariosController extends Controller
{
    public function index()
    {
        $users = User::where('id', '<>', Auth::user()->id);


        $users->with(['roles.statusUser'])->get();
//        $collection = collect($users->with(['roles.statusUser'])->get());
//
//
//        dd($collection->toArray());

        return view('admin.usuarios.index', [
            'users' => $users->paginate(5)
        ]);

    }


    public function pesquisa(Request $request)
    {
        $somente = $request->input('q');

        $users = User::where('id', '<>', Auth::user()->id);

        if ($somente) {
            $users->where('name', $somente);
            $users->orWhere('name', 'like', "%$somente%");
        }

        $users->with('roles');


        return view('admin.usuarios.index', [
            'users' => $users->paginate(),
            'busca' => $somente
        ]);


    }


    public function createIndex()
    {
        return view('admin.usuarios.criacaoUsuario');
    }

    public function create(Request $request)
    {

        $request->validate([
            'name' => 'required|unique:users|max:255',
            'password' => 'required',
            'email' => 'required',
            'role' => 'required'
        ]);


        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $result['success'] = false;
            $result['message'] = 'E-mail informado não valido';
            echo json_encode($result);
            return;
        }

        $roleUser = '';
        switch ($request->role) {
            case 'admin':
                $roleUser = 'admin';
                break;
            case 'operador':
                $roleUser = 'operador';
                break;
            case 'tecnico':
                $roleUser = 'tecnico';
                break;

            default :
                $roleUser = 'operador';
        }

        $user = new User;
        $user->name = $request->name;
        $user->password = Hash::make($request->password, [
            'rounds' => 12,
        ]);
        $user->email = $request->email;
        $user->save();
        $role = new Role;
        $role->type = $roleUser;
        $role->status_id = 1;
        $user->roles()->save($role);


        $result['success'] = true;
        $result['message'] = 'usuario criado com sucesso';
        echo json_encode($result);
        return;
//        return view('admin.usuarios.criacaoUsuario');
    }


    public function edit($id)
    {

        if (!$id) {
            return redirect()->route('admin.usuarios.index');
        }

        $user = User::where('id', $id)->first();

        $user->with(['roles.statusUser'])->get();

        $statusBancos = Status::all();

        return view('admin.usuarios.edicao', [
            'user' => $user,
            'status' => $statusBancos
        ]);
    }

    public function editDo(Request $request, $id)
    {
        $validators = Validator::make($request->all(), [
            'name' => 'required|unique:users|max:255',
            'email' => 'required',
            'roleAtualizar' => 'required',
            'statusUser' => 'required'

        ]);
        if ($validators->fails()) {
            return ['sucess' => false, 'errors' => $validators->errors()->all()];
        }


        try {


            $statusAprovado = 1;

            $user = User::with('roles')->findOrfail($id);

            //Função que exerce atual sera liberado
            $roleAtual = Role::where('user_id', $user->id)->where('status_id', $statusAprovado)->first();
            $roleAtual->status_id = 7;
            $roleAtual->save();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            $role = new Role;
            $role->type = $request->roleAtualizar;
            $role->status_id = $request->statusUser;
            $user->roles()->save($role);
            return ['sucess' => true, 'errors' => []];


        } catch (\Exception $e) {
            return ['sucess' => false, 'errors' => $e->getMessage()];
        }


    }

    public function deleteUser($id)
    {
        $user = User::find($id);

        if (!$user) {
            return ['sucess' => false, 'errors' => ['usuario não encontrado']];

        }

        try {
            $user->delete();
            return redirect()->route('admin.usuarios');
        } catch (\Exception $e) {
            return ['sucess' => false, 'errors' => $e->getMessage()];
        }

    }


}
