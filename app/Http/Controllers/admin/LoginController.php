<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {

        return view('admin.loginAdm');
    }


    public function do(Request $request)
    {

        $data = $request->only(['email', 'password']);

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return redirect()->back()->withInput()->withErrors(['Email invalido']);
        }

        if (Auth::attempt($data)) {

            $user = User::where('id', Auth::user()->id)->first();
            $roles = $user->roles->pluck('type')->toArray();

            if (in_array('admin', $roles)) {

                return redirect('/admin/home');
            } else {
                Auth::logout();
                return redirect()->back()->withInput()->withErrors(['Você não é adm']);

            }
        }

        return redirect()->back()->withInput()->withErrors(['Dados não conferem']);


    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('admin.login');


    }


}
