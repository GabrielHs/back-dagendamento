<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TimesController extends Controller
{
    public function index()
    {
        $times = Team::groupBy('name')->paginate(3);
        return view('admin.times.index', [
            'times' => $times
        ]);

    }


    public function pesquisa(Request $request)
    {
        $somente = $request->input('q');

        if ($somente) {
            $times = Team::groupBy('name')->where('name', $somente);
            $times->orWhere('name', 'like', "%$somente%");
            return view('admin.times.index', [
                'times' => $times->paginate(),
                'busca' => $somente
            ]);

        }

        return view('admin.times.index', [
            'times' => Team::groupBy('name')->paginate(),
            'busca' => $somente
        ]);

    }

    public function createIndex()
    {
        // somente users tecnicos
        $users = User::with('roles')->whereHas('roles', function ($q) {
            $q->where('type', 'tecnico');
        })->get();

        return view('admin.times.criacaoTimes', [
            'users' => $users,
        ]);

    }

    public function createDo(Request $request)
    {
        $validators = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'usersToTime' => 'required',

        ]);
        if ($validators->fails()) {
            return ['sucess' => false, 'errors' => $validators->errors()->all()];
        }

        try {
                $ultimoRegistroTime = Team::latest()->first();

            foreach ($request->input('usersToTime') as $user) {
                $time = new Team;
                $time->name = $request->input('name');
                $time->number_time = $ultimoRegistroTime->number_time + 1;
                $time->user_id = $user;
                $time->save();
            }

            return ['sucess' => true, 'errors' => []];
        } catch (\Exception $e) {
            return ['sucess' => false, 'errors' => $e->getMessage()];
        }
    }

    public function edit($idTeam)
    {
        if (!$idTeam) {
            return ['sucess' => false, 'errors' => 'não encontrado dados'];

        }
        $time = Team::find($idTeam);

        return view('admin.times.EdicaoTimes', [
            'time' => $time,
        ]);

    }


    public function editDo($id, Request $request)
    {
        if (!$id){
            return ['sucess' => false, 'errors' => 'não encontrado informação'];
        }

        $time = Team::find($id);

        $time->name = $request->name;
        $time->save();
        return ['sucess' => true, 'errors' => ''];

    }

    public function deleteTime($id)
    {
        $time = Team::find($id);

        if (!$time) {
            return ['sucess' => false, 'errors' => ['time não encontrado']];

        }

        try {
            $time->delete();
            return redirect()->route('admin.times');
        } catch (\Exception $e) {
            return ['sucess' => false, 'errors' => $e->getMessage()];
        }
    }


}
