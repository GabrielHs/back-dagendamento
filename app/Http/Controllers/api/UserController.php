<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Validator;
use function GuzzleHttp\Promise\all;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return $users->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $exitUser = User::where('email', $request->email)->first();

        if ($exitUser) {
            return ['erro' => 'desculpe, esse e-mail já esta cadastrado na nossa base'];
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password, [
                'rounds' => 12,
            ]),
        ]);


        return ['ok' => $user];
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        $user->load('roles');
        $user->load('teams');
        $user->load('schedulings');


        return ['ok' => $user];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();

            $v = Validator::make($data, [
                'email' => 'required|email',
                'name' => 'required',
            ]);;

            if ($v->fails())
                return ['erros' => $v->errors()];

            $user = User::where('id', $id)->first();

            if (!$user) {
                return ['errors' => 'desculpe, esse e-mail não encontrado na nossa base'];
            }

            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->save();

            return ['ok'];
        } catch (\Exception $e) {
            return ['errors' => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::find($id);

        if (!$user) {
            return ['errors' => 'desculpe, esse e-mail não encontrado na nossa base'];
        }

        if ($id === Auth::user()->id) {
            return ['errors' => 'desculpe, voce não pode realizar essa operacao'];

        }

        try {
            $user->delete();

            return ['ok' => 'Usuario deletado'];

        } catch (\Exception $e) {
            return ['errors' => $e->getMessage()];
        }
    }
}
