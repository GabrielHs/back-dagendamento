<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWTAuthController extends Controller
{


    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $user = User::where('email', $credentials['email'])->first();

        if (!$user) {
            return ['error' => 'Usuário não encontrado'];
        }
        
        if (Hash::check($credentials['password'], $user->password)) {
            $token = JWTAuth::fromUser($user);

            $user['jwt'] = $token;

            return $user;
        }

        return ['error' => 'Sua senha esta incorreta'];
    }


}
