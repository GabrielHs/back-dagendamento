<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'admin/home');


Route::prefix('admin')->group(function () {

    // Dash
    Route::get('login', 'admin\LoginController@login')->name('admin.login');

    Route::post('login', 'admin\LoginController@do')->name('admin.login.do');

    Route::get('sair', 'admin\LoginController@logout')->middleware('auth')->name('admin.logout');

    Route::get('home', 'admin\HomeController@index')->middleware('auth')->name('admin.home');


    // Usuarios
    Route::get('usuarios', 'admin\UsuariosController@index')->middleware('auth')->name('admin.usuarios');

    Route::get('usuarios/pesquisa', 'admin\UsuariosController@pesquisa')->middleware('auth')->name('admin.usuarios.pesquisa');

    Route::get('usuarios/create', 'admin\UsuariosController@createIndex')->middleware('auth')->name('admin.usuarios.create');

    Route::post('usuarios/create', 'admin\UsuariosController@create')->middleware('auth')->name('admin.usuarios.create.do');

    Route::get('usuarios/{id}', 'admin\UsuariosController@edit')->middleware('auth')->name('admin.usuarios.edit');

    Route::post('usuarios/{id}', 'admin\UsuariosController@editDo')->middleware('auth')->name('admin.usuarios.edit.do');

    Route::get('usuarios/delete/{id}', 'admin\UsuariosController@deleteUser')->middleware('auth')->name('admin.usuarios.delete');


    // times == equipes
    Route::get('times', 'admin\TimesController@index')->middleware('auth')->name('admin.times');

    Route::get('times/pesquisa', 'admin\TimesController@pesquisa')->middleware('auth')->name('admin.times.pesquisa');


    Route::get('times/create', 'admin\TimesController@createIndex')->middleware('auth')->name('admin.times.create');

    Route::post('times/create', 'admin\TimesController@createDo')->middleware('auth')->name('admin.times.create.do');

    Route::get('times/{idTeam}', 'admin\TimesController@edit')->middleware('auth')->name('admin.times.edit');

    Route::post('times/{idTeam}', 'admin\TimesController@editDo')->middleware('auth')->name('admin.times.edit.do');

    Route::get('times/delete/{idTeam}', 'admin\TimesController@deleteTime')->middleware('auth')->name('admin.times.delete');


});


